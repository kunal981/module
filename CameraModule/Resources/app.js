// TODO: write your module tests here
var cropimage = require('com.cropimage');
Ti.API.info("module is => " + cropimage);

var image_path = "/image/appicon.png";
var name;

var win = Ti.UI.createWindow({
	backgroundColor : 'white',
	navBarHidden : true,

	width : '100%',
	height : '100%',
	orientationModes : [Ti.UI.PORTRAIT]
});
win.orientationModes = [Ti.UI.PORTRAIT];
var headerView = Ti.UI.createView({
	width : '100%',
	height : '10%',
	top : 0,
	backgroundColor : "#2E2C2C"
});
win.add(headerView);

// Create a Label.
var aLabel = Ti.UI.createLabel({
	text : 'Camera Module',
	color : '#ffffff',
	font : {
		fontSize : 18
	},
	height : Ti.UI.SIZE,
	width : Ti.UI.SIZE,

	textAlign : 'center'
});

// Add to the parent view.
headerView.add(aLabel);

// Create a Button.
var aButton = Ti.UI.createImageView({
	backgroundImage : '/image/icon_back.png',
	height : 31,
	width : 17,

	left : 20
});

// Listen for click events.
aButton.addEventListener('click', function() {
	win.close();
});

// Add to the parent view.
headerView.add(aButton);

var open = Ti.UI.createButton({
	title : 'Pick Image',
	height : '9%',
	width : '35%',
	left : '35%',
	top : '12%',
	borderColor : 'white',
	borderRadius : 7,
	borderWidth : 1,
	backgroundColor : '#2E2C2C',
	color : '#ffffff'
});
win.add(open);

open.addEventListener('click', function() {
	if (open.title == "Show Image") {
		cropimage.start();

		open.title = "Retry";
	} else if (open.title == "Pick Image") {
		cropimage.CameraOpen();
		open.title = "Show Image";
	} else {

		cropimage.CameraOpen();
		open.title = "Show Image";
	}

});
cropimage.addEventListener("Path_Fetch", function(evt) {
	//alert("Image saved to " + evt.path);
	Ti.App.Properties.setString("ONLY_PATH", evt.path);
	Ti.App.Properties.setString("NAME_PATH", evt.path_name);
	name = Ti.App.Properties.getString("NAME_PATH");
	Ti.API.error("Path: " + Ti.App.Properties.getString("ONLY_PATH"));
	Ti.API.error("name: " + name);
	image_path = Ti.App.Properties.getString("ONLY_PATH");
	name = Ti.App.Properties.getString("NAME_PATH");
	Ti.API.error("Path: " + Ti.App.Properties.getString("ONLY_PATH"));
	Ti.API.error("name: " + name);
	image_path = Ti.App.Properties.getString("ONLY_PATH");
	Ti.API.error("image_path: " + image_path);

	Ti.API.error("image_path:: " + image_path);

	if (Ti.Filesystem.isExternalStoragePresent()) {
		Ti.API.error("external storage found" + image_path);
	} else {
		Ti.API.error("No external storage found");

	}
	var dir = Titanium.Filesystem.getFile(Titanium.Filesystem.externalStorageDirectory, name);
	Ti.API.error("name:" + name);
	//alert(dir.getSize() + "--------name========" + dir.getName());
	// Create an ImageView.
	var anImageView = Ti.UI.createImageView({
		backgroundImage : dir.getNativePath(),
		width : '100%',
		height : '70%',
		top : '25%',

	});
	anImageView.addEventListener('load', function() {
		Ti.API.info('Image loaded!');
	});

	win.add(anImageView);

});

win.open();

