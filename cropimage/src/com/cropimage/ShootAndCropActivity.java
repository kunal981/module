package com.cropimage;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import org.appcelerator.kroll.KrollObject;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

public class ShootAndCropActivity extends Activity {
	Bundle extras;
	public static Bitmap thePic;
	public static String Path;
	public static boolean check = false;
	// keep track of camera capture intent
	final int CAMERA_CAPTURE = 1;
	// keep track of cropping intent
	final int PIC_CROP = 2;
	// captured picture uri
	private Uri picUri;
	KrollObject mKrollObject;
	CropimageModule module;
	HashMap<String, Object> event = new HashMap<String, Object>();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		module = new CropimageModule();

		//Log.e("module:", "" + module.example());
		try {
			// use standard intent to capture an image
			Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			// we will handle the returned data in onActivityResult
			startActivityForResult(captureIntent, CAMERA_CAPTURE);
		} catch (ActivityNotFoundException anfe) {
			// display an error message
			String errorMessage = "Whoops - your device doesn't support capturing images!";
			Toast toast = Toast
					.makeText(this, errorMessage, Toast.LENGTH_SHORT);
			toast.show();
		}

	}

	/**
	 * Handle user returning from both capturing and cropping the image
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			// user is returning from capturing an image using the camera
			if (requestCode == CAMERA_CAPTURE) {
				// get the Uri for the captured image
				picUri = data.getData();
				// carry out the crop operation
				performCrop();
			}
			// user is returning from cropping the image
			else if (requestCode == PIC_CROP) {

				Bundle extras = data.getExtras();
				// Log.d("extras:", "" + extras);
				// get the cropped bitmap
				thePic = extras.getParcelable("data");
				// Log.d("thePic:", "" + thePic.toString());
				// test(thePic);
				check = true;
				storeImage(thePic);
				
				//module.Test();
				
				Log.e("ghdd", "DONE DONE");
				
				
				//module.example();
				// storeImage(thePic);

			}
		}
	}

	public String getvalue() {
		return "Amit";
	}

	/**
	 * Helper method to carry out crop operation
	 */
	private void performCrop() {
		// take care of exceptions
		try {
			// call the standard crop action intent (the user device may not
			// support it)
			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			// indicate image type and Uri
			cropIntent.setDataAndType(picUri, "image/*");
			// set crop properties
			cropIntent.putExtra("crop", "true");
			// indicate aspect of desired crop
			cropIntent.putExtra("aspectX", 1);
			cropIntent.putExtra("aspectY", 1);
			// indicate output X and Y
			cropIntent.putExtra("outputX", 256);
			cropIntent.putExtra("outputY", 256);
			// retrieve data on return
			cropIntent.putExtra("return-data", true);

			// start the activity - we handle returning in onActivityResult
			startActivityForResult(cropIntent, PIC_CROP);
		}
		// respond to users whose devices do not support the crop action
		catch (ActivityNotFoundException anfe) {
			// display an error message
			String errorMessage = "Whoops - your device doesn't support the crop action!";
			Toast toast = Toast
					.makeText(this, errorMessage, Toast.LENGTH_SHORT);
			toast.show();
		}
	}

	private boolean storeImage(Bitmap imageData) {
		// get path to external storage (SD card)
		String iconsStoragePath = Environment.getExternalStorageDirectory()
				+ "/com.cameramodule";
		File sdIconStorageDir = new File(iconsStoragePath);

		// create storage directories, if they don't exist
		sdIconStorageDir.mkdirs();

		try {
			// String filePath = sdIconStorageDir.toString() + filename;
			File mFile = new File(sdIconStorageDir, ("/"
					+ System.currentTimeMillis() + ".png"));

			// File mFile=new File(filePath);
			FileOutputStream fileOutputStream = new FileOutputStream(mFile);

			BufferedOutputStream bos = new BufferedOutputStream(
					fileOutputStream);

			// choose another format if PNG doesn't suit you
			imageData.compress(CompressFormat.PNG, 100, bos);

			bos.flush();
			bos.close();

			Log.e("sdIconStorageDir:", "" + mFile.getAbsolutePath());
			Log.e("bos:", "" + bos);
			Log.e("bos:", "" + fileOutputStream);
			Path = mFile.getAbsolutePath();
			
			CropimageModule.setExampleProp(mFile.getAbsolutePath());
			Log.e("Path:", "" + Path);
			Log.e("check:", "" + check);
			//event.put("path", mFile.getAbsolutePath());
			//event.put("path_name", mFile.getName());
			Log.e("enter", "dfshhhdfdgh");
			//module.fireEvent("Path_Fetch", event);
			Log.e("after module", "dfshhhdfdgh");
			Log.e("CALLING", "CALLING");
			finish();
		} catch (FileNotFoundException e) {
			Log.w("TAG", "Error saving image file: " + e.getMessage());
			finish();
			return false;
		} catch (IOException e) {
			Log.w("TAG", "Error saving image file: " + e.getMessage());
			finish();
			return false;

		}
		//finish();
		return true;
	}

}
